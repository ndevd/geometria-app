package aplicacion;

//import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 *
 * @author neodev
 * https://ndevv.github.io/neodev
 */
public class Geometria extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldNombre;

	public static String nombre;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Geometria frame = new Geometria();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	//La aplicación será lanzada desde la clase Principal, que
	//contiene el main

	/**
	 * Create the frame.
	 */
	public Geometria() {
		setTitle("App de Geometría");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 388);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setFocusable(true);
		contentPane.setFocusable(true);
		contentPane.setFocusable(true);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHola = new JLabel("¡HOLA!");
		lblHola.setFont(new Font("Dialog", Font.BOLD, 14));
		lblHola.setForeground(Color.WHITE);
		lblHola.setHorizontalAlignment(SwingConstants.CENTER);
		lblHola.setBounds(207, 12, 195, 29);
		contentPane.add(lblHola);

		JLabel lblNombre = new JLabel("Ingresa tu nombre");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(247, 247, 138, 15);
		contentPane.add(lblNombre);

		textFieldNombre = new JTextField();

		if (nombre != "") {
			textFieldNombre.setText(nombre);
		}

		textFieldNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					nombre = textFieldNombre.getText();
					if (!nombre.isBlank()) {
						Geometria2 ventana2 = new Geometria2();
						ventana2.setVisible(true);
						dispose();
					}
				}
			}
		});
		textFieldNombre.setBackground(Color.LIGHT_GRAY);
		textFieldNombre.setBounds(247, 283, 131, 19);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		//this.dispose();

		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); //////// cursor
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre = textFieldNombre.getText();

				if (!nombre.isBlank()) {

					Geometria2 ventana2 = new Geometria2();
					ventana2.setVisible(true);
					dispose();
					System.out.println("Puntaje actual: "+Geometria3.puntaje);
					
				} else {
					lblNombre.setForeground(Color.RED);
				}
			}
		});

		JLabel lblCop = new JLabel("neodev\u00a9");
		lblCop.setBounds(12, 329, 70, 15);
		contentPane.add(lblCop);
		btnContinuar.setBackground(Color.DARK_GRAY);
		btnContinuar.setForeground(Color.ORANGE);
		btnContinuar.setBounds(254, 314, 117, 25);
		contentPane.add(btnContinuar);

		JLabel lblFondo = new JLabel("");
		lblFondo.setHorizontalAlignment(SwingConstants.CENTER);
		lblFondo.setBackground(Color.LIGHT_GRAY);
		lblFondo.setForeground(Color.WHITE);
		lblFondo.setIcon(new ImageIcon("img/Yashahime.gif"));
		lblFondo.setBounds(0, 0, 621, 356);
		contentPane.add(lblFondo);

		addWindowListener((WindowListener) new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				int result = JOptionPane.showConfirmDialog(null, "¿Deseas salir?", "Cerrar Aplicación: ",
						JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION)
					setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				else if (result == JOptionPane.NO_OPTION)
					setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
		});
	}
}
